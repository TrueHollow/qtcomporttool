# README

QtComPortTool is a simple tool for debugging communication through serial port. It works well on various OS (All that supports Qt 5, tested on Windows 7 and Arch Linux).

## Table Of Contents:

---

[TOC]

---

## Features:

* Multi platform
* Fully customized connection to port
* Can set preferred formats to input/output
* Timeline between data IO

## Support input/output formats:

* ASCII (hELLO WORLD!\r\n)
* HEX (0x0f 0XFa 00 ff)
* DEC (-128 0 127)
* UDEC (0 127 255)

## Screenshots:

![screenshot.png](https://bitbucket.org/repo/pjerg5/images/3273124756-screenshot.png)