@ECHO OFF
cls
echo "========================"
echo "Script starts"
:: Configuration
:: For example
:: SET "QT_DIR=D:\Qt\5.4\mingw491_32"
SET "QT_DIR=D:\Qt\5.4\mingw491_32"
:: release OR debug
SET "BUILD=release"
:: Build project name and platform
SET "PROJECT_NAME=QtComPortTool"
SET "PROJECT_PLATFORM=Desktop"

:: Setting directory var for main project dir and restoring back original work directory
set "THIS_DIR=%cd%"
cd ..
SET "PROJECT_DIR=%cd%"
cd %THIS_DIR%

:: Getting build directory
for /d %%d in (%PROJECT_DIR%\build-%PROJECT_NAME%-%PROJECT_PLATFORM%*%BUILD%) do (
	SET "BUILD_DIR=%%d\%BUILD%"
)

SET "OBJ_EXT=*.o"
SET "CPP_EXT=*.cpp"

echo "Deleting unused files (%OBJ_EXT%,%CPP_EXT%)..."
del %BUILD_DIR%\%OBJ_EXT% /q
del %BUILD_DIR%\%CPP_EXT% /q

echo "Making complete package..."
set "PATH=%QT_DIR%;%QT_DIR%\bin;%PATH%"
windeployqt.exe --%BUILD% %BUILD_DIR%

echo "Coping missing files %BUILD% build"
IF /I %BUILD%==release goto RELEASE
IF /I %BUILD%==debug goto DEBUG
goto UNKNOWN_BUILD

:RELEASE
copy "%QT_DIR%\bin\libgcc_s_dw2-1.dll" "%BUILD_DIR%\libgcc_s_dw2-1.dll"
copy "%QT_DIR%\bin\libwinpthread-1.dll" "%BUILD_DIR%\libwinpthread-1.dll"
copy "%QT_DIR%\bin\libstdc++-6.dll" "%BUILD_DIR%\libstdc++-6.dll
goto END

:DEBUG
:: TODO

goto END
:UNKNOWN_BUILD
echo "Unknown build, nothing to copy"
goto END

:END
echo "Script ends"

pause