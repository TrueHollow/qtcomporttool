<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>ConnectionWidget</name>
    <message>
        <location filename="connectionwidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connectionwidget.ui" line="20"/>
        <source>Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connectionwidget.ui" line="26"/>
        <location filename="connectionwidget.cpp" line="25"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connectionwidget.cpp" line="19"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LogWidget</name>
    <message>
        <location filename="logwidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="logwidget.ui" line="20"/>
        <source>Logging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="logwidget.cpp" line="22"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="logwidget.cpp" line="25"/>
        <source>Select all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="logwidget.cpp" line="32"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="logwidget.cpp" line="39"/>
        <source>Save as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="logwidget.cpp" line="71"/>
        <source>Log successfully saved to %1</source>
        <oldsource>Log successfully saved to </oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="logwidget.cpp" line="73"/>
        <source>File save error: %1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="logwidget.cpp" line="87"/>
        <source>Log files (*.log)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="logwidget.cpp" line="88"/>
        <source>Any files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="logwidget.cpp" line="104"/>
        <source>Selected files count is not equal to one (%1).</source>
        <oldsource>Selected files count is not equal to 1. </oldsource>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>QtComPortTool by Titan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="59"/>
        <source>MainWindow::setCodecsNames names.count() is equal to zero!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Model</name>
    <message>
        <location filename="model.cpp" line="33"/>
        <source>Trying to send empty (zero bytes) data array!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model.cpp" line="36"/>
        <source>Trying to send data in closed port! Data: %1</source>
        <oldsource>Trying to send data in closed port! </oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model.cpp" line="50"/>
        <source>PortName was set to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model.cpp" line="57"/>
        <source>BaudRate was set to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model.cpp" line="60"/>
        <source>BaudRate wasn&apos;t set to new value.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model.cpp" line="66"/>
        <source>Databits was set to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model.cpp" line="69"/>
        <source>Databits wasn&apos;t set to new value.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model.cpp" line="75"/>
        <source>StopBits was set to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model.cpp" line="78"/>
        <source>StopBits wasn&apos;t set to new value.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model.cpp" line="84"/>
        <source>Parity was set to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model.cpp" line="87"/>
        <source>Parity wasn&apos;t set to new value.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model.cpp" line="93"/>
        <source>Flow was set to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model.cpp" line="96"/>
        <source>Flow wasn&apos;t set to new value.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model.cpp" line="106"/>
        <source>Error happend. Error code: %1. Error message: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model.cpp" line="115"/>
        <source>Port was successfully opened.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model.cpp" line="118"/>
        <source>Port wasn&apos;t opened.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model.cpp" line="124"/>
        <source>Port was closed.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Presenter</name>
    <message>
        <location filename="presenter.cpp" line="40"/>
        <source>Presenter::encodeData codecId is out of range - %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="presenter.cpp" line="43"/>
        <source>Current Codec set to %1 (Id: %2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="presenter.cpp" line="52"/>
        <location filename="presenter.cpp" line="75"/>
        <source>currentCodec is not set!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SendWidget</name>
    <message>
        <location filename="sendwidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sendwidget.ui" line="20"/>
        <source>Sending data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sendwidget.ui" line="35"/>
        <source>Send</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsWidget</name>
    <message>
        <location filename="settingswidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="20"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="38"/>
        <source>Port name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="58"/>
        <source>Baudrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="71"/>
        <source>9600</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="81"/>
        <source>Data bits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="92"/>
        <source>5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="97"/>
        <source>6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="102"/>
        <source>7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="107"/>
        <source>8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="115"/>
        <source>Stop bits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="123"/>
        <source>1 stop bit.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="128"/>
        <source>1.5 stop bits.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="133"/>
        <source>2 stop bits.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="141"/>
        <source>Parity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="149"/>
        <location filename="settingswidget.ui" line="185"/>
        <location filename="settingswidget.cpp" line="70"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="154"/>
        <source>Even</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="159"/>
        <source>Odd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="164"/>
        <source>Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="169"/>
        <source>Mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="177"/>
        <source>Flow control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="190"/>
        <source>Hardware (RTS/CTS)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="195"/>
        <source>Software (XON/XOFF)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.cpp" line="63"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.cpp" line="67"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.cpp" line="70"/>
        <source>Is busy?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.cpp" line="70"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.cpp" line="74"/>
        <source>Manufacturer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.cpp" line="78"/>
        <source>Product Identifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.cpp" line="83"/>
        <source>Serial Number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.cpp" line="86"/>
        <source>System Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.cpp" line="89"/>
        <source>Vendor Identifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.cpp" line="94"/>
        <source>&lt;p&gt;No ports found in this system.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.cpp" line="112"/>
        <source>Baudrate value cannot be parsed from lineedit.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.cpp" line="136"/>
        <source>Databits cannot be parsed. Unknown index value.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.cpp" line="156"/>
        <source>Stopbits cannot be parsed. Unknown index value.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.cpp" line="184"/>
        <source>Parity cannot be parsed. Unknown index value.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingswidget.cpp" line="204"/>
        <source>Flowcontrol cannot be parsed. Unknown index value.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TransmissionModel</name>
    <message>
        <location filename="transmissionmodel.cpp" line="36"/>
        <source>Try to combine less than 2 packets!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="transmissionmodel.cpp" line="59"/>
        <source>Remove zero count indexes!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="transmissionmodel.cpp" line="81"/>
        <source>Where is no data to clear!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="transmissionmodel.cpp" line="106"/>
        <location filename="transmissionmodel.cpp" line="123"/>
        <source>PacketDirectionEnum::Unknown was cased!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="transmissionmodel.cpp" line="166"/>
        <source>Packet direction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="transmissionmodel.cpp" line="169"/>
        <source>Decoded data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="transmissionmodel.cpp" line="172"/>
        <source>Time lapse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="transmissionmodel.cpp" line="175"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="transmissionmodel.cpp" line="239"/>
        <source>Packets data successfully saved to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="transmissionmodel.cpp" line="241"/>
        <source>File save error: %1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TransmissionWidget</name>
    <message>
        <location filename="transmissionwidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="transmissionwidget.ui" line="20"/>
        <source>Transmission Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="transmissionwidget.cpp" line="42"/>
        <source>Combine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="transmissionwidget.cpp" line="51"/>
        <source>Delete row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="transmissionwidget.cpp" line="56"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="transmissionwidget.cpp" line="66"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="transmissionwidget.cpp" line="76"/>
        <source>Save as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="transmissionwidget.cpp" line="190"/>
        <source>Log files (*.log)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="transmissionwidget.cpp" line="191"/>
        <source>Any files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="transmissionwidget.cpp" line="207"/>
        <source>Selected files count is not equal to 1. %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
