#ifndef SETTINGSWIDGET_H
#define SETTINGSWIDGET_H

#include <QWidget>
#include <QSerialPort>
#include "logprovider.h"

namespace Ui {
class SettingsWidget;
}

class SettingsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SettingsWidget(QWidget *parent = 0);
    ~SettingsWidget();
signals:
    void portNameChanged(const QString &newValue) const;

    void baudRateChanged(const qint32 &newValue) const;

    void dataBitsChanged(const QSerialPort::DataBits &newValue) const;

    void stopBitsChanged(const QSerialPort::StopBits &newValue) const;

    void parityChanged(const QSerialPort::Parity &newValue) const;

    void flowChanded(const QSerialPort::FlowControl &newValue) const;

private slots:
    void on_lineEditPortName_textChanged(const QString &arg1);

    void on_lineEditBaudrate_textChanged(const QString &arg1);

    void on_comboBoxDatabits_currentIndexChanged(const int &index);

    void on_comboBoxStopbits_currentIndexChanged(const int &index);

    void on_comboBoxParity_currentIndexChanged(const int &index);

    void on_comboBoxFlowcontrol_currentIndexChanged(const int &index);

private:
    Ui::SettingsWidget *ui;

    void initSerialTooltip();

    void initBaudrateTooltip();

    static QString createTableLine(const QString &tdFirst, const QString &tdSecond);
};
#endif // SETTINGSWIDGET_H
