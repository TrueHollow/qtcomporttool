#include "deccodec.h"

DecCodec::DecCodec() : rx("[-]?\\d+")
{
}

DecCodec::~DecCodec()
{
}

QString DecCodec::Name() const
{
    return codecName;
}

QByteArray DecCodec::Encode(const QString &data) const
{
    QByteArray result;
    auto       pos         = 0;
    auto       convertFlag = false;
    int        code;
    char       c;

    while ((pos = rx.indexIn(data, pos)) != -1) {
        code = rx.cap(0).toInt(&convertFlag);
        if (convertFlag == true) {
            if (code < -128 || code > 127) {
                LogProvider::logEventHandler(QString("Variable %1 is out of range!").arg(code), LogProvider::Warning);
            } else {
                c = static_cast <char>(code);
                result.append(c);
            }
        } else {
            LogProvider::logEventHandler(QString("DecCodec::Encode convertFlag == false"), LogProvider::Warning);
        }
        pos += rx.matchedLength();
    }

    return result;
}

QString DecCodec::Decode(const QByteArray &data) const
{
    QString result;

    foreach(const auto & c, data)
    {
        result.append(QString::number(c) + " ");
    }
    return result;
}
