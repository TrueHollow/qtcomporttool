#include "logwidget.h"
#include "ui_logwidget.h"

LogWidget::LogWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LogWidget),
    contextMenu()
{
    ui->setupUi(this);
    auto provider = LogProvider::getSingleton();
    connect(provider, &LogProvider::logEvent, this, &LogWidget::logEventmsg);
    ui->plainTextEditLog->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->plainTextEditLog, &QPlainTextEdit::customContextMenuRequested, this, &LogWidget::customMenuRequested);
    creatingContextMenu();
}

void LogWidget::creatingContextMenu()
{
    QList <QAction *> actions;
    QAction           *action;

    action = new QAction(tr("Copy"), this);
    actions.append(action);

    action = new QAction(tr("Select all"), this);
    actions.append(action);

    action = new QAction(this);
    action->setSeparator(true);
    actions.append(action);

    action = new QAction(tr("Clear"), this);
    actions.append(action);

    action = new QAction(this);
    action->setSeparator(true);
    actions.append(action);

    action = new QAction(tr("Save as"), this);
    actions.append(action);

    contextMenu.addActions(actions);
}

void LogWidget::preparingContextMenu()
{
    const QList <QAction *> actions = contextMenu.actions();

    if (ui->plainTextEditLog->toPlainText().isEmpty()) {
        contextMenu.setDisabled(true);
    } else {
        contextMenu.setEnabled(true);
        return;
    }
    if (ui->plainTextEditLog->textCursor().hasSelection()) {
        actions.at(0)->setEnabled(true);
    } else {
        actions.at(0)->setDisabled(true);
    }
}

void LogWidget::saveFile(const QString &fullPathToFile) const
{
    QFile file;

    file.setFileName(fullPathToFile);
    if (file.open(QIODevice::WriteOnly | QIODevice::Text) == true) {
        QTextStream out(&file);
        out << ui->plainTextEditLog->toPlainText();
        file.close();
        LogProvider::logEventHandler(QString(tr("Log successfully saved to %1")).arg(fullPathToFile));
    } else {
        LogProvider::logEventHandler(QString(tr("File save error: %1 (%2)")).arg(file.errorString()).arg(file.error()), LogProvider::Critical);
    }
}

void LogWidget::saveFileDialog()
{
    QFileDialog dialog;

    dialog.setDirectory(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation));
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.selectFile("log.log");

    QStringList filters;
    filters << tr("Log files (*.log)")
            << tr("Any files (*)");
    dialog.setNameFilters(filters);

    QList <QUrl> urls;
    urls << QUrl::fromLocalFile(QStandardPaths::writableLocation(QStandardPaths::DesktopLocation))
         << QUrl::fromLocalFile(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation))
         << QUrl::fromLocalFile(QStandardPaths::writableLocation(QStandardPaths::HomeLocation));
    dialog.setSidebarUrls(urls);

    //dialog.setOptions(QFileDialog::DontUseNativeDialog);

    dialog.setViewMode(QFileDialog::Detail);
    dialog.setDefaultSuffix("log");
    if (dialog.exec() == QDialog::Accepted) {
        const auto fileNames = dialog.selectedFiles();
        if (fileNames.count() != 1) {
            LogProvider::logEventHandler(QString(tr("Selected files count is not equal to one (%1).")).arg(fileNames.count()), LogProvider::Warning);
        } else {
            saveFile(fileNames.at(0));
        }
    }
}

void LogWidget::customMenuRequested(QPoint pos)
{
    preparingContextMenu();
    const auto position = ui->plainTextEditLog->viewport()->mapToGlobal(pos);
    const auto action   = contextMenu.exec(position);
    const auto actions  = contextMenu.actions();
    if (action == actions.at(0)) {
        ui->plainTextEditLog->copy();
    } else if (action == actions.at(1)) {
        ui->plainTextEditLog->selectAll();
    } else if (action == actions.at(3)) {
        ui->plainTextEditLog->clear();
    } else if (action == actions.at(5)) {
        saveFileDialog();
    }
}

void LogWidget::logEventmsg(const QString &msg, const LogProvider::LogOutputLevel &level)
{
    auto result = QString("<p style='font-family: Courier New,Courier,Lucida Sans Typewriter,Lucida Typewriter,monospace;%1'>%2: %3</p>");

    switch (level) {
    case LogProvider::Debug:
        result = result.arg("color:black;");
        break;

    case LogProvider::Warning:
        result = result.arg("color:#afb42b;");
        break;

    case LogProvider::Critical:
        result = result.arg("color:red;");
        break;

    case LogProvider::Fatal:
        result = result.arg("color:red;font-weight: bold;");
        abort();
    }
    result = result.arg(QTime::currentTime().toString()).arg(msg);
    ui->plainTextEditLog->appendHtml(result);
    QScrollBar *sb = ui->plainTextEditLog->verticalScrollBar();
    sb->setValue(sb->maximum());
}

LogWidget::~LogWidget()
{
    delete ui;
}
