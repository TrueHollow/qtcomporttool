#ifndef ROUTECONTAINER_H
#define ROUTECONTAINER_H

#include <QObject>
#include "presenter.h"
#include "model.h"
#include "mainwindow.h"

class RouteContainer : public QObject
{
    Q_OBJECT
public:
    explicit RouteContainer(QObject *parent = 0);
    ~RouteContainer();

    void Init();

    void Start();

private:
    const Presenter presenter;
    const Model     model;
    MainWindow      view;
};
#endif // ROUTECONTAINER_H
