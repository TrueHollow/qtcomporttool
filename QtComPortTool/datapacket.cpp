#include "datapacket.h"

DataPacket::DataPacket() : rawDataArray(),
    decodedStringData(""),
    packetCreationTime(QDateTime::currentDateTime()),
    directionParameter(PacketDirectionEnum::Unknown)
{
}

DataPacket::~DataPacket()
{
}

void DataPacket::setRawData(const QByteArray &newData)
{
    rawDataArray = newData;
}

QByteArray DataPacket::rawData() const
{
    return rawDataArray;
}

void DataPacket::setDecodedString(const QString &newString)
{
    decodedStringData = newString;
}

QString DataPacket::decodedString() const
{
    return decodedStringData;
}

QDateTime DataPacket::packetCreation() const
{
    return packetCreationTime;
}

DataPacket::PacketDirectionEnum DataPacket::direction() const
{
    return directionParameter;
}

void DataPacket::setPacketDirection(const PacketDirectionEnum &newWay)
{
    directionParameter = newWay;
}

void DataPacket::combine(const DataPacket *anotherPart)
{
    rawDataArray      += anotherPart->rawDataArray;
    decodedStringData += anotherPart->decodedStringData;
}
