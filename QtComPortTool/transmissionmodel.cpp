#include "transmissionmodel.h"

TransmissionModel::TransmissionModel(QObject *parent)
    : QAbstractTableModel(parent),
    red(QColor(255, 205, 210)),
    green(QColor(220, 237, 200))
{
}

TransmissionModel::~TransmissionModel()
{
}

int TransmissionModel::rowCount(const QModelIndex & /*parent*/) const
{
    return packets.count();
}

int TransmissionModel::columnCount(const QModelIndex & /*parent*/) const
{
    return TransmissionModel::COLS;
}

void TransmissionModel::addPacket(const QSharedPointer <DataPacket> &packet)
{
    beginInsertRows(QModelIndex(), packets.count(), packets.count());
    packets.append(packet);
    endInsertRows();
}

void TransmissionModel::combinePackets(const QList <int> &indexes)
{
    const int count = indexes.count();

    if (count < 2) {
        LogProvider::logEventHandler(tr("Try to combine less than 2 packets!"), LogProvider::Warning);
    } else {
        auto rowsId = indexes;
        qSort(rowsId);
        beginResetModel();
        auto first = packets.at(rowsId.first());

        for (int i = 1; i < rowsId.count(); ++i) {
            first->combine(packets.at(rowsId.at(i)).data());
        }
        for (int i = count - 1; i >= 1; --i) {
            packets.removeAt(rowsId.at(i));
        }

        endResetModel();
    }
}

void TransmissionModel::removePackets(const QList <int> &indexes)
{
    const int count = indexes.count();

    if (count == 0) {
        LogProvider::logEventHandler(tr("Remove zero count indexes!"), LogProvider::Warning);
    } else {
        if (count > 1) {
            auto rowsId = indexes;
            qSort(rowsId);
            beginResetModel();
            for (int i = count - 1; i >= 0; --i) {
                packets.removeAt(rowsId.at(i));
            }
            endResetModel();
        } else {
            const auto &index = indexes.at(0);
            beginRemoveRows(QModelIndex(), index, index);
            packets.removeAt(index);
            endRemoveRows();
        }
    }
}

void TransmissionModel::clearPacketList()
{
    if (packets.count() == 0) {
        LogProvider::logEventHandler(tr("Where is no data to clear!"), LogProvider::Warning);
        return;
    }
    beginRemoveRows(QModelIndex(), 0, packets.count() - 1);
    packets.clear();
    endRemoveRows();
}

QVariant TransmissionModel::data(const QModelIndex &index, int role) const
{
    const int row    = index.row();
    const int column = index.column();

    switch (role) {
    case Qt::BackgroundRole:
        if (column == 2) {
            DataPacket::PacketDirectionEnum direction = packets.at(row)->direction();
            switch (direction) {
            case DataPacket::PacketDirectionEnum::Sent:
                return red;

            case DataPacket::PacketDirectionEnum::Received:
                return green;

            case DataPacket::PacketDirectionEnum::Unknown:
                LogProvider::logEventHandler(tr("PacketDirectionEnum::Unknown was cased!"), LogProvider::Warning);
                break;
            }
        }

        break;

    case Qt::DisplayRole:
        switch (column) {
        case 0:
            return QString::number(row + 1);

        case 1:
        {
            const auto direction = packets.at(row)->direction();
            switch (direction) {
            case DataPacket::PacketDirectionEnum::Unknown:
                LogProvider::logEventHandler(tr("PacketDirectionEnum::Unknown was cased!"), LogProvider::Warning);
                break;

            case DataPacket::PacketDirectionEnum::Sent:
                return QString("Tx:");

            case DataPacket::PacketDirectionEnum::Received:
                return QString("Rx:");
            }
            break;
        }

        case 2:
            return packets.at(row)->decodedString();

        case 3:
            if (row == 0) {
                return QString("0");
            } else {
                QDateTime prevTime    = packets.at(row - 1)->packetCreation();
                QDateTime currentTime = packets.at(row)->packetCreation();
                qint64    elapsed     = prevTime.msecsTo(currentTime);
                return getTimeElapsed(elapsed);
            }

        case 4:
            return packets.at(row)->packetCreation().toString("hh:mm:ss:zzz");
        }
        break;
    }
    return QVariant();
}

QVariant TransmissionModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (orientation == Qt::Horizontal) {
            switch (section) {
            case 0:
                return QString("#");

            case 1:
                return QString(tr("Packet direction"));

            case 2:
                return QString(tr("Decoded data"));

            case 3:
                return QString(tr("Time lapse"));

            case 4:
                return QString(tr("Time"));
            }
        }
        break;

    case Qt::FontRole:
    {
        QFont bold;
        bold.setBold(true);
        return bold;
    }
    }

    return QVariant();
}

QString TransmissionModel::getTimeElapsed(const qint64 &elapsedMilliseconds) const
{
    QString result;

    if (elapsedMilliseconds < TransmissionModel::OneSecond) {
        result = QString("%1 ms").arg(QString::number(elapsedMilliseconds));
    } else if (elapsedMilliseconds < TransmissionModel::OneMinute) {
        auto seconds = static_cast <double> (elapsedMilliseconds) / TransmissionModel::OneSecond;
        result = QString("%1 s").arg(QString::number(seconds));
    } else {
        auto minutes = elapsedMilliseconds / TransmissionModel::OneMinute;
        auto seconds = static_cast <double>(elapsedMilliseconds % TransmissionModel::OneMinute) / TransmissionModel::OneSecond;
        result = QString("%1 m, %2 s").arg(QString::number(minutes)).arg(QString::number(seconds));
    }
    return result;
}

void TransmissionModel::savePacketsToFile(const QString &fullPathToFile) const
{
    QFile file;

    file.setFileName(fullPathToFile);
    if (file.open(QIODevice::WriteOnly | QIODevice::Text) == true) {
        QString     buffer;
        QTextStream out(&file);
        out << "#\tDecoded data\tPacket direction\tTime\n";
        const int count = packets.count();
        QSharedPointer <DataPacket> packet;
        for (int i = 0; i < count; ++i) {
            packet = packets.at(i);
            buffer = QString("%1\t%2\t%3\t%4\n").arg(i).arg(packet->decodedString());
            switch (packet->direction()) {
            case DataPacket::Unknown:
                buffer = buffer.arg("Unknown");
                break;

            case DataPacket::Sent:
                buffer = buffer.arg("Tx");
                break;

            case DataPacket::Received:
                buffer = buffer.arg("Rx");
                break;
            }
            buffer = buffer.arg(packet->packetCreation().toString("hh:mm:ss:zzz"));
            out << buffer;
        }
        file.close();
        LogProvider::logEventHandler(QString(tr("Packets data successfully saved to %1")).arg(fullPathToFile));
    } else {
        LogProvider::logEventHandler(QString(tr("File save error: %1 (%2)")).arg(file.errorString()).arg(file.error()), LogProvider::Critical);
    }
}
