#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "settingswidget.h"
#include "connectionwidget.h"
#include "sendwidget.h"
#include "transmissionwidget.h"
#include "logwidget.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void setCodecsNames(const QStringList &names);

signals:
    void portNameChanged(const QString &newValue) const;

    void baudRateChanged(const qint32 &newValue) const;

    void dataBitsChanged(const QSerialPort::DataBits &newValue) const;

    void stopBitsChanged(const QSerialPort::StopBits &newValue) const;

    void parityChanged(const QSerialPort::Parity &newValue) const;

    void flowChanded(const QSerialPort::FlowControl &newValue) const;

    void sendData(const QString &data) const;

    void changeCurrectCodecId(const int &newCodecId) const;

    void openPort();

    void closePort();

public slots:
    void portOpened();

    void portClosed();

    void packetProcess(const QSharedPointer <DataPacket> &packet);

private:
    Ui::MainWindow *ui;

    SettingsWidget *settingsWidget;

    ConnectionWidget *connectionWidget;

    SendWidget *sendWidget;

    TransmissionWidget *transmissionWidget;

    LogWidget *logWidget;
};
#endif // MAINWINDOW_H
