#include "hexcodec.h"

HexCodec::HexCodec() : rx("(?:0[xX])?[0-9a-fA-F]{2}")
{
}

HexCodec::~HexCodec()
{
}

QString HexCodec::Name() const
{
    return codecName;
}

QByteArray HexCodec::Encode(const QString &data) const
{
    QByteArray result;
    auto       pos         = 0;
    auto       convertFlag = false;
    uint       code;
    char       c;

    while ((pos = rx.indexIn(data, pos)) != -1) {
        code = rx.cap(0).toUInt(&convertFlag, 16);
        if (convertFlag == true) {
            if (code > 255) {
                LogProvider::logEventHandler(QString("Variable %1 is out of range!").arg(code), LogProvider::Warning);
            } else {
                c = static_cast <char>(code);
                result.append(c);
            }
        } else {
            LogProvider::logEventHandler(QString("DecCodec::Encode convertFlag == false"), LogProvider::Warning);
        }
        pos += rx.matchedLength();
    }

    return result;
}

QString HexCodec::Decode(const QByteArray &data) const
{
    QString result;
    int     code;

    foreach(const auto & c, data)
    {
        code = static_cast <int>(c);
        if (code < 0) {
            code += 256;
        }
        QString codeString = QString::number(code, 16);
        if (codeString.count() == 1) {
            codeString.prepend('0');
        }
        codeString.append(' ');
        result.append(codeString);
    }
    return result;
}
