#include "connectionwidget.h"
#include "ui_connectionwidget.h"

ConnectionWidget::ConnectionWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ConnectionWidget),
    isOpened(false)
{
    ui->setupUi(this);
}

ConnectionWidget::~ConnectionWidget()
{
    delete ui;
}

void ConnectionWidget::portOpened()
{
    ui->pushButtonConnect->setText(tr("Close"));
    isOpened = true;
}

void ConnectionWidget::portClosed()
{
    ui->pushButtonConnect->setText(tr("Open"));
    isOpened = false;
}

void ConnectionWidget::on_pushButtonConnect_clicked()
{
    if (isOpened == false) {
        emit openPort();
    } else {
        emit closePort();
    }
}
