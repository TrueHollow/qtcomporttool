#include "sendwidget.h"
#include "ui_sendwidget.h"

SendWidget::SendWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SendWidget)
{
    ui->setupUi(this);
    // For working then Enter pressed
    connect(ui->lineEditData, &QLineEdit::returnPressed, ui->pushButtonSend, &QPushButton::click);
}

SendWidget::~SendWidget()
{
    delete ui;
}

void SendWidget::on_pushButtonSend_clicked()
{
    if (ui->lineEditData->text().isEmpty() == false) {
        emit sendData(ui->lineEditData->text());
    }
}

void SendWidget::setCodecsNames(const QStringList &names)
{
    // In case of multy usage.
    ui->comboBoxCodecs->clear();
    ui->comboBoxCodecs->addItems(names);
}

void SendWidget::on_comboBoxCodecs_currentIndexChanged(const int &index)
{
    if (index != -1) {
        emit changeCurrectCodecId(index);
    }
}

void SendWidget::portOpened()
{
    ui->pushButtonSend->setEnabled(true);
}

void SendWidget::portClosed()
{
    ui->pushButtonSend->setEnabled(false);
}
