#ifndef ASCIICODEC_H
#define ASCIICODEC_H

#include <QRegExp>
#include "icodec.h"

class AsciiCodec : public ICodec
{
public:
    AsciiCodec();
    ~AsciiCodec();

    QString Name() const;

    QByteArray Encode(const QString &data) const;

    QString Decode(const QByteArray &data) const;

private:
    const QString codecName = "Ascii Codec";

    const QString carriageReturn        = QString("\\r");
    const QString carriageReturnReplace = QString(QChar(QChar::CarriageReturn));

    const QString lineFeed        = QString("\\n");
    const QString lineFeedReplace = QString(QChar(QChar::LineFeed));

    const QString tabulation        = QString("\\t");
    const QString tabulationReplace = QString(QChar(QChar::Tabulation));
};
#endif // ASCIICODEC_H
