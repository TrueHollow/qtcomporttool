#ifndef UDECCODEC_H
#define UDECCODEC_H

#include "icodec.h"
#include <QRegExp>
#include "logprovider.h"

class UDecCodec : public ICodec
{
public:
    UDecCodec();
    ~UDecCodec();

    QString Name() const;

    QByteArray Encode(const QString &data) const;

    QString Decode(const QByteArray &data) const;

private:
    const QString codecName = "Unsigned Char Codec";

    const QRegExp rx;
};
#endif // UDECCODEC_H
