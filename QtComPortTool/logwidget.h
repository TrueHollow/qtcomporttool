#ifndef LOGWIDGET_H
#define LOGWIDGET_H

#include <QWidget>
#include <QMenu>
#include <QClipboard>
#include <QFileDialog>
#include <QStandardPaths>
#include <QTime>
#include <QScrollBar>
#include <QFile>
#include <QTextStream>
#include "logprovider.h"

namespace Ui {
class LogWidget;
}

class LogWidget : public QWidget
{
    Q_OBJECT

public:
    explicit LogWidget(QWidget *parent = 0);
    ~LogWidget();

public slots:
    void customMenuRequested(QPoint pos);

private:
    Ui::LogWidget *ui;

    QMenu contextMenu;

    void preparingContextMenu();

    void creatingContextMenu();

    void saveFileDialog();

    void saveFile(const QString &fullPathToFile) const;

private slots:
    void logEventmsg(const QString &msg, const LogProvider::LogOutputLevel &level);
};
#endif // LOGWIDGET_H
