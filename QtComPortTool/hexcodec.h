#ifndef HEXCODEC_H
#define HEXCODEC_H

#include "icodec.h"
#include <QRegExp>

class HexCodec : public ICodec
{
public:
    HexCodec();
    ~HexCodec();

    QString Name() const;

    QByteArray Encode(const QString &data) const;

    QString Decode(const QByteArray &data) const;

private:
    const QString codecName = "Hex Codec";

    const QRegExp rx;
};
#endif // HEXCODEC_H
