#include "udeccodec.h"

UDecCodec::UDecCodec() : rx("\\d+")
{
}

UDecCodec::~UDecCodec()
{
}

QString UDecCodec::Name() const
{
    return codecName;
}

QByteArray UDecCodec::Encode(const QString &data) const
{
    QByteArray result;
    auto       pos         = 0;
    auto       convertFlag = false;
    uint       code;
    char       c;

    while ((pos = rx.indexIn(data, pos)) != -1) {
        code = rx.cap(0).toUInt(&convertFlag);
        if (convertFlag == true) {
            if (code > 255) {
                LogProvider::logEventHandler(QString("Variable %1 is out of range!").arg(code), LogProvider::Warning);
            } else {
                c = static_cast <char>(code);
                result.append(c);
            }
        } else {
            LogProvider::logEventHandler(QString("DecCodec::Encode convertFlag == false"), LogProvider::Warning);
        }
        pos += rx.matchedLength();
    }

    return result;
}

QString UDecCodec::Decode(const QByteArray &data) const
{
    QString result;
    int     code;

    foreach(const auto & c, data)
    {
        code = static_cast <int>(c);
        if (code < 0) {
            code += 256;
        }
        result.append(QString::number(code) + " ");
    }
    return result;
}
