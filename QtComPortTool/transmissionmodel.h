#ifndef TRANSMISSIONMODEL_H
#define TRANSMISSIONMODEL_H

#include <QAbstractTableModel>
#include <QBrush>
#include <QFont>
#include <QFile>
#include <QTextStream>
#include "datapacket.h"
#include "logprovider.h"

class TransmissionModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit TransmissionModel(QObject *parent = 0);
    ~TransmissionModel();

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;

    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

    void addPacket(const QSharedPointer <DataPacket> &packet);

    void removePackets(const QList <int> &indexes);

    void clearPacketList();

    void combinePackets(const QList <int> &indexes);

    // Column count
    static constexpr int COLS = 5;

    static constexpr qint64 OneSecond = 1000;

    static constexpr qint64 OneMinute = 60 * OneSecond;

    void savePacketsToFile(const QString &fullPathToFile) const;

private:
    QList <QSharedPointer <DataPacket> > packets;

    QString getTimeElapsed(const qint64 &elapsedMilliseconds) const;

    const QBrush red;

    const QBrush green;
};
#endif // TRANSMISSIONMODEL_H
