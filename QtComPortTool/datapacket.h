#ifndef DATAPACKET_H
#define DATAPACKET_H

#include <QTime>

class DataPacket
{
public:
    DataPacket();
    ~DataPacket();

    enum PacketDirectionEnum
    {
        Unknown,
        Sent,
        Received
    };

    QByteArray rawData() const;

    void setRawData(const QByteArray &newData);

    QString decodedString() const;

    void setDecodedString(const QString &newString);

    QDateTime packetCreation() const;

    PacketDirectionEnum direction() const;

    void setPacketDirection(const PacketDirectionEnum &newWay);

    void combine(const DataPacket *anotherPart);

private:
    QByteArray rawDataArray;

    QString decodedStringData;

    const QDateTime packetCreationTime;

    PacketDirectionEnum directionParameter;
};
#endif // DATAPACKET_H
