#include "transmissionwidget.h"
#include "ui_transmissionwidget.h"

TransmissionWidget::TransmissionWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TransmissionWidget),
    model(this),
    contextMenu()
{
    ui->setupUi(this);

    ui->tableView->setModel(&model);
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->tableView->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
    ui->tableView->horizontalHeader()->setSectionsMovable(true);

    ui->tableView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->tableView, &QTableView::customContextMenuRequested, this, &TransmissionWidget::customMenuRequested);
    creatingContextMenu();

    QShortcut *shortcut = new QShortcut(QKeySequence(QKeySequence::Delete), ui->tableView);
    connect(shortcut, &QShortcut::activated, this, &TransmissionWidget::deleteSelectedRows);

    shortcut = new QShortcut(QKeySequence(QKeySequence::Copy), ui->tableView);
    connect(shortcut, &QShortcut::activated, this, &TransmissionWidget::copySelected);
}

TransmissionWidget::~TransmissionWidget()
{
    delete ui;
}

void TransmissionWidget::addPacket(const QSharedPointer <DataPacket> &packet)
{
    model.addPacket(packet);
}

void TransmissionWidget::creatingContextMenu()
{
    QList <QAction *> actions;
    QAction           *action;

    // 0
    action = new QAction(tr("Combine"), this);
    actions.append(action);

    // 1
    action = new QAction(this);
    action->setSeparator(true);
    actions.append(action);

    // 2
    action = new QAction(tr("Delete row"), this);
    action->setShortcut(QKeySequence(QKeySequence::Delete));
    actions.append(action);

    // 3
    action = new QAction(tr("Clear"), this);
    action->setShortcut(QKeySequence(QKeySequence::DeleteCompleteLine));
    actions.append(action);

    // 4
    action = new QAction(this);
    action->setSeparator(true);
    actions.append(action);

    // 5
    action = new QAction(tr("Copy"), this);
    action->setShortcut(QKeySequence(QKeySequence::Copy));
    actions.append(action);

    // 6
    action = new QAction(this);
    action->setSeparator(true);
    actions.append(action);

    // 7
    action = new QAction(tr("Save as"), this);
    action->setShortcut(QKeySequence(QKeySequence::SaveAs));
    actions.append(action);

    contextMenu.addActions(actions);
}

void TransmissionWidget::preparingContextMenu()
{
    const int totalRowsCount             = ui->tableView->model()->rowCount();
    const QItemSelectionModel *selection = ui->tableView->selectionModel();
    const QModelIndexList     indexes    = selection->selectedIndexes();

    if (totalRowsCount == 0) {
        contextMenu.setDisabled(true);
        return;
    } else {
        contextMenu.setEnabled(true);
    }

    const QList <QAction *> actions = contextMenu.actions();
    bool      flagOperationsOnSelected;
    const int count = indexes.count();
    if (count == 0) {
        flagOperationsOnSelected = false;
    } else {
        flagOperationsOnSelected = true;
    }
    if (count > 1) {
        actions.at(0)->setVisible(true);
    } else {
        actions.at(0)->setVisible(false);
    }
    actions.at(2)->setVisible(flagOperationsOnSelected);
    actions.at(5)->setVisible(flagOperationsOnSelected);
}

QList <int> TransmissionWidget::getSelectedRowsId(const QModelIndexList &indexes) const
{
    QList <int>     rowsId;
    QModelIndexList indexesLocal = indexes;
    QModelIndex     previous     = indexesLocal.first();
    indexesLocal.removeFirst();
    rowsId.append(previous.row());
    int row;

    foreach(const auto & current, indexesLocal)
    {
        row = current.row();
        if (row != previous.row()) {
            rowsId.append(row);
            previous = current;
        }
    }
    return rowsId;
}

void TransmissionWidget::deleteSelectedRows()
{
    const QItemSelectionModel *selection = ui->tableView->selectionModel();
    const QModelIndexList     indexes    = selection->selectedIndexes();

    if (indexes.count() == 0) {
        return;
    }
    model.removePackets(getSelectedRowsId(indexes));
}

void TransmissionWidget::combineSelected()
{
    const QItemSelectionModel *selection = ui->tableView->selectionModel();
    const QModelIndexList     indexes    = selection->selectedIndexes();

    if (indexes.count() < 2) {
        return;
    }
    model.combinePackets(getSelectedRowsId(indexes));
}

void TransmissionWidget::customMenuRequested(QPoint pos)
{
    preparingContextMenu();
    const QPoint            position = ui->tableView->viewport()->mapToGlobal(pos);
    const QAction           *action  = contextMenu.exec(position);
    const QList <QAction *> actions  = contextMenu.actions();

    if (action == actions.at(0)) {
        combineSelected();
    } else if (action == actions.at(2)) {
        deleteSelectedRows();
    } else if (action == actions.at(3)) {
        model.clearPacketList();
    } else if (action == actions.at(5)) {
        copySelected();
    } else if (action == actions.at(7)) {
        saveTableViewToFile();
    }
}

void TransmissionWidget::copySelected() const
{
    QApplication::clipboard()->setText(getSelectedData());
}

void TransmissionWidget::saveTableViewToFile() const
{
    QFileDialog dialog;

    dialog.setDirectory(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation));
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.selectFile("transmision.log");

    QStringList filters;
    filters << tr("Log files (*.log)")
            << tr("Any files (*)");
    dialog.setNameFilters(filters);

    QList <QUrl> urls;
    urls << QUrl::fromLocalFile(QStandardPaths::writableLocation(QStandardPaths::DesktopLocation))
         << QUrl::fromLocalFile(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation))
         << QUrl::fromLocalFile(QStandardPaths::writableLocation(QStandardPaths::HomeLocation));
    dialog.setSidebarUrls(urls);

    //dialog.setOptions(QFileDialog::DontUseNativeDialog);

    dialog.setViewMode(QFileDialog::Detail);
    dialog.setDefaultSuffix("log");
    if (dialog.exec() == QDialog::Accepted) {
        QStringList fileNames = dialog.selectedFiles();
        if (fileNames.count() != 1) {
            LogProvider::logEventHandler(QString(tr("Selected files count is not equal to 1. %1")).arg(fileNames.count()), LogProvider::Warning);
        } else {
            model.savePacketsToFile(fileNames.at(0));
        }
    }
}

QString TransmissionWidget::getSelectedData() const
{
    QString result;
    const QItemSelectionModel *selection = ui->tableView->selectionModel();
    const QModelIndexList     indexes    = selection->selectedIndexes();

    QModelIndex previous = indexes.first();

    foreach(const auto & current, indexes)
    {
        QVariant data = model.data(current);
        QString  text = data.toString();

        if (current.row() != previous.row()) {
            // Override last tab,
            result.replace(result.length() - 1, 1, '\n');
        }
        result.append(text);
        result.append('\t');
        previous = current;
    }
    // Remove last char (New line or tab)
    result.remove(result.length() - 1, 1);
    return result;
}
