#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    settingsWidget = new SettingsWidget(this);
    ui->verticalLayoutLeft->addWidget(settingsWidget);

    connectionWidget = new ConnectionWidget(this);
    ui->verticalLayoutLeft->addWidget(connectionWidget);

    logWidget = new LogWidget(this);
    ui->verticalLayoutLeft->addWidget(logWidget);

    sendWidget = new SendWidget(this);
    ui->verticalLayoutRight->addWidget(sendWidget);

    transmissionWidget = new TransmissionWidget(this);
    ui->verticalLayoutRight->addWidget(transmissionWidget);

    connect(settingsWidget, &SettingsWidget::portNameChanged, this, &MainWindow::portNameChanged);
    connect(settingsWidget, &SettingsWidget::baudRateChanged, this, &MainWindow::baudRateChanged);
    connect(settingsWidget, &SettingsWidget::dataBitsChanged, this, &MainWindow::dataBitsChanged);
    connect(settingsWidget, &SettingsWidget::stopBitsChanged, this, &MainWindow::stopBitsChanged);
    connect(settingsWidget, &SettingsWidget::parityChanged, this, &MainWindow::parityChanged);
    connect(settingsWidget, &SettingsWidget::flowChanded, this, &MainWindow::flowChanded);

    connect(connectionWidget, &ConnectionWidget::openPort, this, &MainWindow::openPort);
    connect(connectionWidget, &ConnectionWidget::closePort, this, &MainWindow::closePort);

    connect(sendWidget, &SendWidget::sendData, this, &MainWindow::sendData);
    connect(sendWidget, &SendWidget::changeCurrectCodecId, this, &MainWindow::changeCurrectCodecId);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::portOpened()
{
    connectionWidget->portOpened();
    sendWidget->portOpened();
}

void MainWindow::portClosed()
{
    connectionWidget->portClosed();
    sendWidget->portClosed();
}

void MainWindow::setCodecsNames(const QStringList &names)
{
    if (names.count() == 0) {
        LogProvider::logEventHandler(tr("MainWindow::setCodecsNames names.count() is equal to zero!"), LogProvider::Critical);
    } else {
        sendWidget->setCodecsNames(names);
    }
}

void MainWindow::packetProcess(const QSharedPointer <DataPacket> &packet)
{
    transmissionWidget->addPacket(packet);
}
