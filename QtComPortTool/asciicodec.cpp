#include "asciicodec.h"

AsciiCodec::AsciiCodec() : ICodec()
{
}

AsciiCodec::~AsciiCodec()
{
}

QString AsciiCodec::Name() const
{
    return codecName;
}

QByteArray AsciiCodec::Encode(const QString &data) const
{
    auto result = data;

    result = result.replace(carriageReturn, carriageReturnReplace);
    result = result.replace(lineFeed, lineFeedReplace);
    result = result.replace(tabulation, tabulationReplace);

    return result.toLatin1();
}

QString AsciiCodec::Decode(const QByteArray &data) const
{
    auto result = QString::fromLatin1(data);

    result = result.replace(carriageReturnReplace, carriageReturn);
    result = result.replace(lineFeedReplace, lineFeed);
    result = result.replace(tabulationReplace, tabulation);

    return result;
}
