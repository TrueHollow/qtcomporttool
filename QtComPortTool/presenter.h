#ifndef PRESENTER_H
#define PRESENTER_H

#include <QObject>
#include <QSharedPointer>
#include <QStringList>
#include "icodec.h"
#include "asciicodec.h"
#include "deccodec.h"
#include "udeccodec.h"
#include "hexcodec.h"
#include "datapacket.h"
#include "logprovider.h"

class Presenter : public QObject
{
    Q_OBJECT
public:
    explicit Presenter(QObject *parent = 0);
    ~Presenter();

    QStringList GetCodecsNames() const;

signals:
    void dataEncoded(const QByteArray &data) const;

    void dataDecoded(const QSharedPointer <DataPacket> &packet) const;

public slots:

    void encodeData(const QString &data) const;

    void setCurrentCodecId(const int &codecId);

    void decodeReceivedData(const QByteArray &dataIn) const;

    void decodeSentData(const QByteArray &dataOut) const;

private:
    void decodeData(const QByteArray &rawData, const DataPacket::PacketDirectionEnum &direction) const;

    QList <ICodec *> codecs;

    int currentCodecId;
};
#endif // PRESENTER_H
