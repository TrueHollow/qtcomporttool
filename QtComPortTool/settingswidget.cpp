#include "settingswidget.h"
#include "ui_settingswidget.h"

#include <QtSerialPort/QSerialPortInfo>

SettingsWidget::SettingsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingsWidget)
{
    ui->setupUi(this);
    initSerialTooltip();
    initBaudrateTooltip();
    ui->lineEditBaudrate->setValidator(new QIntValidator(0, 128000, this));
}

SettingsWidget::~SettingsWidget()
{
    delete ui;
}

QString SettingsWidget::createTableLine(const QString &tdFirst, const QString &tdSecond)
{
    return QString("<tr><td style=\"font-weight: bold;\">%1:</td><td>%2</td></tr>")
              .arg(tdFirst)
              .arg(tdSecond);
}

void SettingsWidget::initBaudrateTooltip()
{
    QString buffer;

    buffer.append("<p style=\"font-weight: bold;\">Value</p>");
    buffer.append("<p>1200</p>");
    buffer.append("<p>2400</p>");
    buffer.append("<p>4800</p>");
    buffer.append("<p>9600</p>");
    buffer.append("<p>19200</p>");
    buffer.append("<p>38400</p>");
    buffer.append("<p>57600</p>");
    buffer.append("<p>115200</p>");
    ui->lineEditBaudrate->setToolTip(buffer);
}

void SettingsWidget::initSerialTooltip()
{
    QString buffer;
    QString tempString;

    const auto ports = QSerialPortInfo::availablePorts();
    const auto count = ports.count();

    if (count > 0) {
        buffer.append("<table>");

        for (auto i = 0; i < count; ++i) {
            const auto &info = ports[i];
            if (i != 0) {
                buffer.append("<tr><td colspan=\"2\"><hr /></td></tr>");
            } else {
                ui->lineEditPortName->setText(info.portName());
            }
            // Name
            buffer.append(createTableLine(tr("Name"), info.portName()));
            // Description
            tempString = info.description();
            if (tempString.isEmpty() == false) {
                buffer.append(createTableLine(tr("Description"), tempString));
            }
            // IsBusy ?
            buffer.append(createTableLine(tr("Is busy?"), info.isBusy() ? tr("Yes") : tr("No")));
            // Manufacturer
            tempString = info.manufacturer();
            if (tempString.isEmpty() == false) {
                buffer.append(createTableLine(tr("Manufacturer"), tempString));
            }
            // ProductIdentifier
            if (info.hasProductIdentifier()) {
                buffer.append(createTableLine(tr("Product Identifier"), QString::number(info.productIdentifier())));
            }
            // SerialNumber
            tempString = info.serialNumber();
            if (tempString.isEmpty() == false) {
                buffer.append(createTableLine(tr("Serial Number"), tempString));
            }
            // SystemLocation
            buffer.append(createTableLine(tr("System Location"), info.systemLocation()));
            // VendorIdentifier
            if (info.hasVendorIdentifier()) {
                buffer.append(createTableLine(tr("Vendor Identifier"), QString::number(info.vendorIdentifier())));
            }
        }
        buffer.append("</table>");
    } else {
        buffer.append(tr("<p>No ports found in this system.</p>"));
    }
    ui->lineEditPortName->setToolTip(buffer);
}

void SettingsWidget::on_lineEditPortName_textChanged(const QString &newValue)
{
    emit portNameChanged(newValue);
}

void SettingsWidget::on_lineEditBaudrate_textChanged(const QString &newValue)
{
    auto   ok       = true;
    qint32 baudrate = newValue.toInt(&ok);

    if (ok == true) {
        emit baudRateChanged(baudrate);
    } else {
        LogProvider::logEventHandler(tr("Baudrate value cannot be parsed from lineedit."), LogProvider::Warning);
    }
}

void SettingsWidget::on_comboBoxDatabits_currentIndexChanged(const int &index)
{
    switch (index) {
    case 0:
        emit dataBitsChanged(QSerialPort::Data5);
        break;

    case 1:
        emit dataBitsChanged(QSerialPort::Data6);
        break;

    case 2:
        emit dataBitsChanged(QSerialPort::Data7);
        break;

    case 3:
        emit dataBitsChanged(QSerialPort::Data8);
        break;

    default:
        LogProvider::logEventHandler(tr("Databits cannot be parsed. Unknown index value."), LogProvider::Warning);
    }
}

void SettingsWidget::on_comboBoxStopbits_currentIndexChanged(const int &index)
{
    switch (index) {
    case 0:
        emit stopBitsChanged(QSerialPort::OneStop);
        break;

    case 1:
        emit stopBitsChanged(QSerialPort::OneAndHalfStop);
        break;

    case 2:
        emit stopBitsChanged(QSerialPort::TwoStop);
        break;

    default:
        LogProvider::logEventHandler(tr("Stopbits cannot be parsed. Unknown index value."), LogProvider::Warning);
    }
}

void SettingsWidget::on_comboBoxParity_currentIndexChanged(const int &index)
{
    switch (index) {
    case 0:
        emit parityChanged(QSerialPort::NoParity);
        break;

    case 1:
        emit parityChanged(QSerialPort::EvenParity);
        break;

    case 2:
        emit parityChanged(QSerialPort::OddParity);
        break;

    case 3:
        emit parityChanged(QSerialPort::SpaceParity);
        break;

    case 4:
        emit parityChanged(QSerialPort::MarkParity);
        break;

    default:
        LogProvider::logEventHandler(tr("Parity cannot be parsed. Unknown index value."), LogProvider::Warning);
    }
}

void SettingsWidget::on_comboBoxFlowcontrol_currentIndexChanged(const int &index)
{
    switch (index) {
    case 0:
        emit flowChanded(QSerialPort::NoFlowControl);
        break;

    case 1:
        emit flowChanded(QSerialPort::HardwareControl);
        break;

    case 2:
        emit flowChanded(QSerialPort::SoftwareControl);
        break;

    default:
        LogProvider::logEventHandler(tr("Flowcontrol cannot be parsed. Unknown index value."), LogProvider::Warning);
    }
}
