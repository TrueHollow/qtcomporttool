#ifndef SENDWIDGET_H
#define SENDWIDGET_H

#include <QWidget>

namespace Ui {
class SendWidget;
}

class SendWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SendWidget(QWidget *parent = 0);
    ~SendWidget();

    void setCodecsNames(const QStringList &names);

    void portOpened();

    void portClosed();

signals:

    void changeCurrectCodecId(const int &newCodecId);

    void sendData(const QString &data) const;

private slots:
    void on_pushButtonSend_clicked();

    void on_comboBoxCodecs_currentIndexChanged(const int &index);

private:
    Ui::SendWidget *ui;
};
#endif // SENDWIDGET_H
