#ifndef TRANSMISSIONWIDGET_H
#define TRANSMISSIONWIDGET_H

#include <QApplication>
#include <QWidget>
#include <QMenu>
#include <QClipboard>
#include <QFileDialog>
#include <QStandardPaths>
#include <QShortcut>
#include <QScrollBar>
#include "transmissionmodel.h"
#include "datapacket.h"
#include "logprovider.h"

namespace Ui {
class TransmissionWidget;
}

class TransmissionWidget : public QWidget
{
    Q_OBJECT

public:
    explicit TransmissionWidget(QWidget *parent = 0);
    ~TransmissionWidget();

    void addPacket(const QSharedPointer <DataPacket> &packet);

public slots:
    void customMenuRequested(QPoint pos);

private:
    Ui::TransmissionWidget *ui;

    TransmissionModel model;

    QMenu contextMenu;

    void creatingContextMenu();

    void preparingContextMenu();

    QString getSelectedData() const;

    void saveTableViewToFile() const;

    QList <int> getSelectedRowsId(const QModelIndexList &indexes) const;

private slots:
    void deleteSelectedRows();

    void copySelected() const;

    void combineSelected();
};
#endif // TRANSMISSIONWIDGET_H
