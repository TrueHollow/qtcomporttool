#ifndef ICODEC_H
#define ICODEC_H

#include <QObject>
#include "logprovider.h"

class ICodec
{
public:
    ICodec();
    virtual ~ICodec();

    virtual QString Name() const = 0;

    virtual QByteArray Encode(const QString &data) const = 0;

    virtual QString Decode(const QByteArray &data) const = 0;
};
#endif // ICODEC_H
