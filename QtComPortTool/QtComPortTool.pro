#-------------------------------------------------
#
# Project created by QtCreator 2015-03-13T22:36:37
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtComPortTool
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    presenter.cpp \
    settingswidget.cpp \
    model.cpp \
    routecontainer.cpp \
    connectionwidget.cpp \
    icodec.cpp \
    asciicodec.cpp \
    sendwidget.cpp \
    logwidget.cpp \
    transmissionwidget.cpp \
    transmissionmodel.cpp \
    datapacket.cpp \
    deccodec.cpp \
    logprovider.cpp \
    udeccodec.cpp \
    hexcodec.cpp

HEADERS  += mainwindow.h \
    presenter.h \
    settingswidget.h \
    model.h \
    routecontainer.h \
    connectionwidget.h \
    icodec.h \
    asciicodec.h \
    sendwidget.h \
    logwidget.h \
    transmissionwidget.h \
    transmissionmodel.h \
    datapacket.h \
    deccodec.h \
    logprovider.h \
    udeccodec.h \
    hexcodec.h

FORMS    += mainwindow.ui \
    settingswidget.ui \
    connectionwidget.ui \
    sendwidget.ui \
    logwidget.ui \
    transmissionwidget.ui

CONFIG += c++14

TRANSLATIONS += QtComPortTool_ru.ts \
    QtComPortTool_en.ts

VERSION = 0.0.1

win32: {
    # icon for the app
    RC_ICONS = resources/icons/app.ico
    QMAKE_TARGET_COMPANY = "t company"
    QMAKE_TARGET_DESCRIPTION = "t desc"
    QMAKE_TARGET_COPYRIGHT = "t copy"
    QMAKE_TARGET_PRODUCT = "t product"
}

DISTFILES += \
    uncrustify.cfg
