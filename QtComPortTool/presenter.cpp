#include "presenter.h"

Presenter::Presenter(QObject *parent) : QObject(parent), codecs()
{
    codecs.append(new AsciiCodec());
    codecs.append(new DecCodec());
    codecs.append(new UDecCodec());
    codecs.append(new HexCodec());

    // This is important in possible future, but now this code is useless
    if (codecs.count() > 0) {
        currentCodecId = 0;
    } else {
        currentCodecId = -1;
    }
}

Presenter::~Presenter()
{
    foreach(auto & codec, codecs)
    {
        delete codec;
    }
}

QStringList Presenter::GetCodecsNames() const
{
    QStringList result;

    foreach(const auto & codec, codecs)
    {
        result.append(codec->Name());
    }
    return result;
}

void Presenter::setCurrentCodecId(const int &codecId)
{
    if (codecId < 0 || codecId >= codecs.count()) {
        LogProvider::logEventHandler(QString(tr("Presenter::encodeData codecId is out of range - %1")).arg(codecId), LogProvider::Warning);
    } else {
        currentCodecId = codecId;
        LogProvider::logEventHandler(QString(tr("Current Codec set to %1 (Id: %2)"))
                                        .arg(codecs.at(currentCodecId)->Name())
                                        .arg(currentCodecId));
    }
}

void Presenter::encodeData(const QString &data) const
{
    if (currentCodecId == -1) {
        LogProvider::logEventHandler(QString(tr("currentCodec is not set!")), LogProvider::Critical);
        return;
    }
    const auto codec = codecs.at(currentCodecId);

    //qDebug() << "Encode: " << data;
    QByteArray result = codec->Encode(data);
    emit       dataEncoded(result);
}

void Presenter::decodeReceivedData(const QByteArray &dataIn) const
{
    decodeData(dataIn, DataPacket::PacketDirectionEnum::Received);
}

void Presenter::decodeSentData(const QByteArray &dataOut) const
{
    decodeData(dataOut, DataPacket::PacketDirectionEnum::Sent);
}

void Presenter::decodeData(const QByteArray &rawData, const DataPacket::PacketDirectionEnum &direction) const
{
    if (currentCodecId == -1) {
        LogProvider::logEventHandler(tr("currentCodec is not set!"), LogProvider::Critical);
        return;
    }
    const auto codec = codecs.at(currentCodecId);
    const QSharedPointer <DataPacket> packet = QSharedPointer <DataPacket>(new DataPacket);

    //qDebug() << "Decode: " << rawData;
    packet->setRawData(rawData);
    packet->setDecodedString(codec->Decode(rawData));
    packet->setPacketDirection(direction);
    emit dataDecoded(packet);
}
