#ifndef LOGPROVIDER_H
#define LOGPROVIDER_H

#include <QObject>

#ifdef QT_DEBUG
#include <QDebug>
#endif

class LogProvider : public QObject
{
    Q_OBJECT
public:
    ~LogProvider();

    enum LogOutputLevel
    {
        Debug, Warning, Critical, Fatal
    };

    static void logEventHandler(const QString &msg, const LogOutputLevel &level = LogOutputLevel::Debug)
    {
        auto provider = getSingleton();
        emit provider->logEvent(msg, level);

#ifdef QT_DEBUG
        switch (level) {
        case LogOutputLevel::Debug:
            qDebug() << msg;
            break;

        case LogOutputLevel::Warning:
            qWarning() << msg;
            break;

        case LogOutputLevel::Critical:
            qCritical() << msg;
            break;

        case LogOutputLevel::Fatal:
            qFatal(msg.toStdString().c_str());
            break;
        }
#endif

    }

    static LogProvider *getSingleton()
    {
        return LogProvider::logger;
    }

signals:
    void logEvent(const QString &msg, const LogOutputLevel &level);

private:
    explicit LogProvider(QObject *parent = 0);

    static LogProvider *logger;
};
#endif // LOGPROVIDER_H
