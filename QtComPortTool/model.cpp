#include "model.h"

Model::Model(QObject *parent) : QObject(parent),
    port()
{
    auto ports = QSerialPortInfo::availablePorts();

    if (ports.length() > 0) {
        port.setPortName(ports[0].portName());
    }
    port.setBaudRate(QSerialPort::Baud9600);
    port.setDataBits(QSerialPort::Data8);
    port.setStopBits(QSerialPort::OneStop);
    port.setParity(QSerialPort::NoParity);
    port.setFlowControl(QSerialPort::NoFlowControl);
    connect(&port, SIGNAL(error(QSerialPort::SerialPortError)), this, SLOT(errorLogger(QSerialPort::SerialPortError)));
    connect(&port, &QSerialPort::readyRead, this, &Model::dataReceive);
    connect(&port, &QSerialPort::aboutToClose, this, &Model::portClosing);
}

Model::~Model()
{
}

void Model::dataSend(const QByteArray &dataOut)
{
    if (port.isOpen()) {
        if (dataOut.count() > 0) {
            LogProvider::logEventHandler(QString("Tx: %1").arg(QString::fromLatin1(dataOut)));
            port.write(dataOut);
            emit dataWasSent(dataOut);
        } else {
            LogProvider::logEventHandler(tr("Trying to send empty (zero bytes) data array!"), LogProvider::Warning);
        }
    } else {
        LogProvider::logEventHandler(QString(tr("Trying to send data in closed port! Data: %1")).arg(QString::fromLatin1(dataOut)), LogProvider::Warning);
    }
}

void Model::dataReceive()
{
    auto data = port.readAll();

    LogProvider::logEventHandler(QString("Rx: %1").arg(QString::fromLatin1(data)));
    emit dataReceived(data);
}

void Model::setPortName(const QString &newName)
{
    LogProvider::logEventHandler(QString(tr("PortName was set to %1")).arg(newName));

    port.setPortName(newName);
}

void Model::setBaudRate(const qint32 &newValue)
{
    LogProvider::logEventHandler(QString(tr("BaudRate was set to %1")).arg(newValue));

    if (port.setBaudRate(newValue) == false) {
        LogProvider::logEventHandler(tr("BaudRate wasn't set to new value."), LogProvider::Warning);
    }
}

void Model::setDataBits(const QSerialPort::DataBits &newValue)
{
    LogProvider::logEventHandler(QString(tr("Databits was set to %1")).arg(newValue));

    if (port.setDataBits(newValue) == false) {
        LogProvider::logEventHandler(tr("Databits wasn't set to new value."), LogProvider::Warning);
    }
}

void Model::setStopBits(const QSerialPort::StopBits &newValue)
{
    LogProvider::logEventHandler(QString(tr("StopBits was set to %1")).arg(newValue));

    if (port.setStopBits(newValue) == false) {
        LogProvider::logEventHandler(tr("StopBits wasn't set to new value."), LogProvider::Warning);
    }
}

void Model::setParity(const QSerialPort::Parity &newValue)
{
    LogProvider::logEventHandler(QString(tr("Parity was set to %1")).arg(newValue));

    if (port.setParity(newValue) == false) {
        LogProvider::logEventHandler(tr("Parity wasn't set to new value."), LogProvider::Warning);
    }
}

void Model::setFlow(const QSerialPort::FlowControl &newValue)
{
    LogProvider::logEventHandler(QString(tr("Flow was set to %1")).arg(newValue));

    if (port.setFlowControl(newValue) == false) {
        LogProvider::logEventHandler(tr("Flow wasn't set to new value."), LogProvider::Warning);
    }
}

void Model::errorLogger(const QSerialPort::SerialPortError &error) const
{
    if (error == QSerialPort::NoError) {
        // Ignore all "not errors"
        return;
    }
    LogProvider::logEventHandler(QString(tr("Error happend. Error code: %1. Error message: %2")).arg(error).arg(port.errorString()), LogProvider::Critical);
}

void Model::openPort()
{
    if (port.isOpen()) {
        return;
    }
    if (port.open(QIODevice::ReadWrite)) {
        LogProvider::logEventHandler(tr("Port was successfully opened."));
        emit portOpened();
    } else {
        LogProvider::logEventHandler(tr("Port wasn't opened."));
    }
}

void Model::portClosing()
{
    LogProvider::logEventHandler(tr("Port was closed."));
    emit portClosed();
}

void Model::closePort()
{
    if (port.isOpen()) {
        port.close();
    }
}
