#ifndef MODEL_H
#define MODEL_H

#include <QObject>
#include <QSerialPort>
#include <QSerialPortInfo>
#include "logprovider.h"

class Model : public QObject
{
    Q_OBJECT
public:
    explicit Model(QObject *parent = 0);
    ~Model();

signals:
    void portOpened();
    void portClosed();

    void dataReceived(const QByteArray &dataIn);
    void dataWasSent(const QByteArray &dataOut);

public slots:
    // Setting port state
    void setPortName(const QString &newName);
    void setBaudRate(const qint32 &newValue);
    void setDataBits(const QSerialPort::DataBits &newValue);
    void setStopBits(const QSerialPort::StopBits &newValue);
    void setParity(const QSerialPort::Parity &newValue);
    void setFlow(const QSerialPort::FlowControl &newValue);

    void openPort();
    void closePort();

    void dataSend(const QByteArray &dataOut);

private:
    QSerialPort port;

private slots:
    void errorLogger(const QSerialPort::SerialPortError &error) const;

    void dataReceive();

    void portClosing();
};
#endif // MODEL_H
