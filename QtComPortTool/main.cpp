#include <QApplication>
#include "routecontainer.h"
#include "logprovider.h"
#include <QMap>
#include <QTranslator>

void parseCommangLine(const int &argc, char *argv[])
{
    if (argc == 1) {
        return;
    }
    for (int i = 0; i < argc; ++i) {
        qDebug() << argv[i];
    }
}

QTranslator qtTranslator;

void setLocale(QMap <QString, QString> config = QMap <QString, QString>())
{
    QString       localeFileName;
    const QString keyLocale = QString("LOCALE");

    if (config.contains(keyLocale)) {
        localeFileName = "qt_" + config.value(keyLocale);
    } else {
        localeFileName = "qt_" + QLocale::system().name();
    }
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    parseCommangLine(argc, argv);
    setLocale();
    a.installTranslator(&qtTranslator);
    RouteContainer routeContainer;

    routeContainer.Init();
    routeContainer.Start();
    return a.exec();
}
