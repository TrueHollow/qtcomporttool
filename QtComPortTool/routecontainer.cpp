#include "routecontainer.h"

RouteContainer::RouteContainer(QObject *parent) : QObject(parent),
    presenter(),
    model(),
    view()
{
}

RouteContainer::~RouteContainer()
{
}

void RouteContainer::Init()
{
    connect(&view, &MainWindow::portNameChanged, &model, &Model::setPortName);
    connect(&view, &MainWindow::baudRateChanged, &model, &Model::setBaudRate);
    connect(&view, &MainWindow::dataBitsChanged, &model, &Model::setDataBits);
    connect(&view, &MainWindow::stopBitsChanged, &model, &Model::setStopBits);
    connect(&view, &MainWindow::parityChanged, &model, &Model::setParity);
    connect(&view, &MainWindow::flowChanded, &model, &Model::setFlow);

    connect(&model, &Model::portOpened, &view, &MainWindow::portOpened);
    connect(&model, &Model::portClosed, &view, &MainWindow::portClosed);
    connect(&model, &Model::dataWasSent, &presenter, &Presenter::decodeSentData);
    connect(&model, &Model::dataReceived, &presenter, &Presenter::decodeReceivedData);

    connect(&view, &MainWindow::openPort, &model, &Model::openPort);
    connect(&view, &MainWindow::closePort, &model, &Model::closePort);

    view.setCodecsNames(presenter.GetCodecsNames());
    connect(&view, &MainWindow::sendData, &presenter, &Presenter::encodeData);
    connect(&view, &MainWindow::changeCurrectCodecId, &presenter, &Presenter::setCurrentCodecId);

    connect(&presenter, &Presenter::dataEncoded, &model, &Model::dataSend);
    connect(&presenter, &Presenter::dataDecoded, &view, &MainWindow::packetProcess);
}

void RouteContainer::Start()
{
    view.show();
}
