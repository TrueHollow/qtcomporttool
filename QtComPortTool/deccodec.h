#ifndef DECCODEC_H
#define DECCODEC_H

#include "icodec.h"
#include <QRegExp>

class DecCodec : public ICodec
{
public:
    DecCodec();
    ~DecCodec();

    QString Name() const;

    QByteArray Encode(const QString &data) const;

    QString Decode(const QByteArray &data) const;

private:
    const QString codecName = "Signed Char Codec";

    const QRegExp rx;
};
#endif // DECCODEC_H
