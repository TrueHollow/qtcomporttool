#ifndef CONNECTIONWIDGET_H
#define CONNECTIONWIDGET_H

#include <QWidget>

namespace Ui {
class ConnectionWidget;
}

class ConnectionWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ConnectionWidget(QWidget *parent = 0);
    ~ConnectionWidget();

public slots:
    void portOpened();

    void portClosed();

signals:
    void openPort();

    void closePort();

private:
    Ui::ConnectionWidget *ui;
    bool isOpened;

private slots:
    void on_pushButtonConnect_clicked();
};
#endif // CONNECTIONWIDGET_H
